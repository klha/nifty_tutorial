# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2013-2021 Max-Planck-Society
#
# NIFTy is being developed at the Max-Planck-Institut fuer Astrophysik.

import nifty7 as ift
from helpers import (checkerboard_response, generate_gaussian_data,
                     plot_prior_samples_2d, plot_reconstruction_2d)

position_space = ift.RGSpace(2*(256,))

args = {
    'offset_mean': 0,
    'offset_std': (1e-3, 1e-6),

    # Amplitude of field fluctuations
    'fluctuations': (1., 0.8),  # 1.0, 1e-2

    # Exponent of power law power spectrum component
    'loglogavgslope': (-3., 1),  # -6.0, 1

    # Amplitude of integrated Wiener process power spectrum component
    'flexibility': (2, 1.),  # 1.0, 0.5

    # How ragged the integrated Wiener process component is
    'asperity': (0.5, 0.4)  # 0.1, 0.5
}

signal = ift.SimpleCorrelatedField(position_space, **args)
