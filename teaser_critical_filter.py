# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2013-2021 Max-Planck-Society
#
# NIFTy is being developed at the Max-Planck-Institut fuer Astrophysik.

from functools import reduce
from operator import add

import nifty7 as ift

from helpers import generate_mysterious_data, plot_WF, power_plot

position_space = ift.RGSpace(256)
harmonic_space = position_space.get_default_codomain()

HT = ift.HarmonicTransformOperator(harmonic_space, target=position_space)

power_space = ift.PowerSpace(harmonic_space)

# Set up prior model for the field for the field
# We want to set up a model for the power spectrum with some magic numbers
args = {
    'offset_mean': 0,
    'offset_std': (1e-3, 1e-6),

    # Amplitude of field fluctuations
    'fluctuations': (1., 0.8),  # 1.0, 1e-2

    # Exponent of power law power spectrum component
    'loglogavgslope': (-3., 1),  # -6.0, 1

    # Amplitude of integrated Wiener process power spectrum component
    'flexibility': (2, 1.),  # 1.0, 0.5

    # How ragged the integrated Wiener process component is
    'asperity': (0.5, 0.4)  # 0.1, 0.5
}
correlated_field = ift.SimpleCorrelatedField(position_space, **args)
pspec = correlated_field.power_spectrum

# Set up specific scenario
R = ift.GeometryRemover(position_space)
data_space = R.target

signal_response = R(correlated_field)

# Set up likelihood and load data
N = ift.ScalingOperator(data_space, 0.1)

data, ground_truth = generate_mysterious_data(position_space)
data = ift.makeField(data_space, data)

likelihood = (ift.GaussianEnergy(mean=data, inverse_covariance=N.inverse)
              @ signal_response)

# Solve problem
ic_sampling = ift.GradientNormController(iteration_limit=100)
ic_newton = ift.GradInfNormController(name='Newton', tol=1e-6,
                                      iteration_limit=10)
minimizer = ift.NewtonCG(ic_newton)

H = ift.StandardHamiltonian(likelihood, ic_sampling)

initial_mean = ift.MultiField.full(H.domain, 0.)
mean = initial_mean

# Number of samples used to estimate the KL
N_samples = 10

# Draw new samples to approximate the KL ten times
for _ in range(10):
    # Draw new samples and minimize KL
    KL = ift.MetricGaussianKL(mean, H, N_samples, True)
    KL, convergence = minimizer(KL)
    mean = KL.position

# Plot posterior samples
ground_truth = ift.makeField(position_space, ground_truth)
posterior_samples = [correlated_field(KL.position+samp) for samp in KL.samples]
mean = 0.*posterior_samples[0]
for p in posterior_samples:
    mean = mean + p/len(posterior_samples)
plot_WF('teaser_unknown_power', ground_truth, data, m=mean,
        samples=posterior_samples)

# Plot the reconstruction of the power spectrum
ground_truth_spectrum = ift.PS_field(power_space,
                                     lambda k: 5/((7**2 - k**2)**2 + 3**2*k**2))
posterior_power_samples = [pspec.force(KL.position+samp) for samp in KL.samples]
power_mean = reduce(add, posterior_power_samples)
power_plot('teaser_power_reconstruction', ground_truth_spectrum, power_mean,
           posterior_power_samples)
