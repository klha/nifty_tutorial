# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2013-2021 Max-Planck-Society
#
# NIFTy is being developed at the Max-Planck-Institut fuer Astrophysik.

import nifty7 as ift
from helpers import generate_wf_data, plot_WF

# Want to implement: m = Dj = (S^{-1} + R^T N^{-1} R)^{-1} R^T N^{-1} d

position_space = ift.RGSpace(256)

prior_spectrum = lambda k: 1/(10. + k**2.5)
data, ground_truth = generate_wf_data(position_space, prior_spectrum)

ground_truth = ift.makeField(position_space, ground_truth)
data_space = ift.UnstructuredDomain(data.shape)
data = ift.makeField(data_space, data)
plot_WF('1_data', ground_truth, data)

h_space = position_space.get_default_codomain()
HT = ift.HarmonicTransformOperator(h_space, target=position_space)

# Operators
Sh = ift.create_power_operator(h_space, power_spectrum=prior_spectrum)
R = ift.GeometryRemover(position_space) @ HT

# Fields and data
N = ift.ScalingOperator(data_space, 0.1)
j = R.adjoint(N.inverse(data))

IC = ift.GradientNormController(iteration_limit=100, tol_abs_gradnorm=1e-7)
# WienerFilterCurvature is (R.adjoint*N.inverse*R + Sh.inverse) plus some handy
# helper methods.
curv = ift.WienerFilterCurvature(R, N, Sh, iteration_controller=IC,
                                 iteration_controller_sampling=IC)
D = curv.inverse

m = D(j)
plot_WF('1_result', ground_truth, data, HT(m))

N_samples = 10
samples = [HT(D.draw_sample() + m) for i in range(N_samples)]
plot_WF('1_result_with_uncertainty', ground_truth, data, m=HT(m), samples=samples)
