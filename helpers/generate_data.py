# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2013-2021 Max-Planck-Society
#
# NIFTy is being developed at the Max-Planck-Institut fuer Astrophysik.

import numpy as np

import nifty7 as ift


def generate_gaussian_data(signal_response, noise_covariance):
    ground_truth = ift.from_random(signal_response.domain)
    d = signal_response(ground_truth) + noise_covariance.draw_sample_with_dtype(np.float64)
    return d, ground_truth


def generate_poisson_data(signal_response):
    ground_truth = ift.from_random(signal_response.domain)
    rate = signal_response(ground_truth).val
    d = np.random.poisson(rate)
    return ift.makeField(signal_response.target, d), ground_truth


def generate_bernoulli_data(signal_response):
    ground_truth = ift.from_random(signal_response.domain)
    rate = signal_response(ground_truth).val
    d = np.random.binomial(1, rate)
    return ift.makeField(signal_response.target, d), ground_truth


def generate_wf_data(domain, spectrum):
    harmonic_space = domain.get_default_codomain()
    HT = ift.HarmonicTransformOperator(harmonic_space, target=domain)
    N = ift.ScalingOperator(domain, 0.1)
    S_k = ift.create_power_operator(harmonic_space, spectrum)
    s = HT(S_k.draw_sample_with_dtype(np.float64)).val
    d = s + N.draw_sample_with_dtype(np.float64).val
    return d, s


def generate_mysterious_data(domain):
    return generate_wf_data(domain, lambda k: 5/((7**2 - k**2)**2 + 3**2*k**2))
