# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2013-2021 Max-Planck-Society
#
# NIFTy is being developed at the Max-Planck-Institut fuer Astrophysik.

import helpers as h
import nifty7 as ift

seeds = [123, 42]
name = ['bernoulli', 'poisson']
for mode in [0, 1]:
    ift.random.push_sseq_from_seed(seeds[mode])

    position_space = ift.RGSpace([256, 256])
    harmonic_space = position_space.get_default_codomain()
    power_space = ift.PowerSpace(harmonic_space)
    HT = ift.HarmonicTransformOperator(harmonic_space, target=position_space)

    args = {
        'offset_mean': 0,
        'offset_std': (1e-3, 1e-6),

        # Amplitude of field fluctuations
        'fluctuations': (1., 0.8),  # 1.0, 1e-2

        # Exponent of power law power spectrum component
        'loglogavgslope': (-3., 1),  # -6.0, 1

        # Amplitude of integrated Wiener process power spectrum component
        'flexibility': (2, 1.),  # 1.0, 0.5

        # How ragged the integrated Wiener process component is
        'asperity': (0.5, 0.4)  # 0.1, 0.5
    }
    correlated_field = ift.SimpleCorrelatedField(position_space, **args)
    pspec = correlated_field.power_spectrum

    dct = {}
    if mode == 0:
        signal = correlated_field.sigmoid()
        R = h.checkerboard_response(position_space)
    else:
        signal = correlated_field.exp()
        R = h.exposure_response(position_space)
    h.plot_prior_samples_2d(5, signal, R, correlated_field, pspec, name[mode],
                            **dct)
    signal_response = R @ signal
    if mode == 0:
        signal_response = signal_response.clip(1e-5, 1 - 1e-5)
        data, ground_truth = h.generate_bernoulli_data(signal_response)
        likelihood = ift.BernoulliEnergy(data) @ signal_response
    else:
        data, ground_truth = h.generate_poisson_data(signal_response)
        likelihood = ift.PoissonianEnergy(data) @ signal_response

    # Solve inference problem
    ic_sampling = ift.GradientNormController(iteration_limit=100)
    ic_newton = ift.GradInfNormController(name='Newton', tol=1e-6,
                                          iteration_limit=10)
    minimizer = ift.NewtonCG(ic_newton)
    H = ift.StandardHamiltonian(likelihood, ic_sampling)
    initial_mean = ift.MultiField.full(H.domain, 0.)
    mean = initial_mean
    n_rounds = 5
    for ii in range(n_rounds):
        # Draw new samples and minimize KL
        KL = ift.MetricGaussianKL(mean, H, 30 if ii == n_rounds-1 else 5, True)
        KL, convergence = minimizer(KL)
        mean = KL.position
    h.plot_reconstruction_2d(data, ground_truth, KL, signal, R, pspec,
                             name[mode])
